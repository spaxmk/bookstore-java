package bookstore;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

@SuppressWarnings("serial")
public class addForm extends JFrame {

	private JPanel contentPane;
	private JTextField txtNaziv;
	private JTextField txtAutor;
	private JTextField txtGodIzdavanja;
	private JTextField txtZanr;
	private JTextField txtCena;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					addForm frame = new addForm();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public addForm() {
		setTitle("Dodavanje knjige");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 248, 352);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		dbHelper dataHelper = new dbHelper();
		
		JLabel lblNaziv = new JLabel("Naziv:");
		lblNaziv.setBounds(10, 11, 46, 14);
		contentPane.add(lblNaziv);
		
		txtNaziv = new JTextField();
		txtNaziv.setBounds(10, 28, 215, 20);
		contentPane.add(txtNaziv);
		txtNaziv.setColumns(10);
		
		JLabel lblAutor = new JLabel("Autor:");
		lblAutor.setBounds(10, 62, 46, 14);
		contentPane.add(lblAutor);
		
		txtAutor = new JTextField();
		txtAutor.setBounds(10, 82, 215, 20);
		contentPane.add(txtAutor);
		txtAutor.setColumns(10);
		
		JLabel lblGodIzdavanja = new JLabel("Godina Izdavanja:");
		lblGodIzdavanja.setBounds(10, 113, 110, 14);
		contentPane.add(lblGodIzdavanja);
		
		txtGodIzdavanja = new JTextField();
		txtGodIzdavanja.setBounds(10, 138, 212, 20);
		contentPane.add(txtGodIzdavanja);
		txtGodIzdavanja.setColumns(10);
		
		JLabel lblZanr = new JLabel("Zanr:");
		lblZanr.setBounds(10, 169, 46, 14);
		contentPane.add(lblZanr);
		
		txtZanr = new JTextField();
		txtZanr.setBounds(10, 194, 212, 20);
		contentPane.add(txtZanr);
		txtZanr.setColumns(10);
		
		JLabel lblCena = new JLabel("Cena");
		lblCena.setBounds(10, 225, 46, 14);
		contentPane.add(lblCena);
		
		txtCena = new JTextField();
		txtCena.setBounds(10, 250, 215, 20);
		contentPane.add(txtCena);
		txtCena.setColumns(10);
		
		JButton btnAddBook = new JButton("Dodaj");
		btnAddBook.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				if(txtNaziv.getText().trim().isEmpty()
						||txtAutor.getText().trim().isEmpty()
						||txtGodIzdavanja.getText().trim().isEmpty()
						||txtZanr.getText().trim().isEmpty()
					    ||txtCena.getText().trim().isEmpty())
					JOptionPane.showMessageDialog(null, "Morate popuniti sva pnudjena polja!");
				else {
					try {
						bookHelper book = new bookHelper(txtNaziv.getText().trim(), txtAutor.getText().trim(), txtGodIzdavanja.getText().trim(), txtZanr.getText().trim(), txtCena.getText().trim());
						String nazivProvera = txtNaziv.getText().trim();
						if(dataHelper.provera(nazivProvera)==false) {
							dataHelper.addNew(book);
							JOptionPane.showMessageDialog(null, "Knjiga je uspesno dodata!");
							dispose();
						}
						else
							JOptionPane.showMessageDialog(null, "Knjiga postoji u bazi!");
					}
					catch (Exception ex) {
						ex.printStackTrace();
						JOptionPane.showMessageDialog(null, "Greska pri unosu!");
					}
				}
				
			}
		});
		
		btnAddBook.setBounds(10, 279, 89, 23);
		contentPane.add(btnAddBook);
		
		JButton btnClose = new JButton("Izadji");
		btnClose.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				dispose();
			}
		});
		btnClose.setBounds(136, 279, 89, 23);
		contentPane.add(btnClose);
		
		
	}
}
