package bookstore;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

@SuppressWarnings("serial")
public class editFrame extends JFrame {

	private JPanel contentPane;
	private JTextField txtNaziv;
	private JTextField txtAutor;
	private JTextField txtGodIzdavanja;
	private JTextField txtZanr;
	private JTextField txtCena;
	private dbHelper dataHelper;
	private DefaultTableModel dftm;
	private String bookId;
	private JTextField txtId;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					addForm frame = new addForm();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public editFrame(String id) {
		setTitle("Izmena knjige");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 248, 352);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		//dbHelper dataHelper = new dbHelper();
		
		JLabel lblNaziv = new JLabel("Naziv:");
		lblNaziv.setBounds(10, 11, 46, 14);
		contentPane.add(lblNaziv);
		
		txtNaziv = new JTextField();
		txtNaziv.setBounds(10, 28, 215, 20);
		contentPane.add(txtNaziv);
		txtNaziv.setColumns(10);
		
		JLabel lblAutor = new JLabel("Autor:");
		lblAutor.setBounds(10, 62, 46, 14);
		contentPane.add(lblAutor);
		
		txtAutor = new JTextField();
		txtAutor.setBounds(10, 82, 215, 20);
		contentPane.add(txtAutor);
		txtAutor.setColumns(10);
		
		JLabel lblGodIzdavanja = new JLabel("Godina Izdavanja:");
		lblGodIzdavanja.setBounds(10, 113, 110, 14);
		contentPane.add(lblGodIzdavanja);
		
		txtGodIzdavanja = new JTextField();
		txtGodIzdavanja.setBounds(10, 138, 212, 20);
		contentPane.add(txtGodIzdavanja);
		txtGodIzdavanja.setColumns(10);
		
		JLabel lblZanr = new JLabel("Zanr:");
		lblZanr.setBounds(10, 169, 46, 14);
		contentPane.add(lblZanr);
		
		txtZanr = new JTextField();
		txtZanr.setBounds(10, 194, 212, 20);
		contentPane.add(txtZanr);
		txtZanr.setColumns(10);
		
		JLabel lblCena = new JLabel("Cena");
		lblCena.setBounds(10, 225, 46, 14);
		contentPane.add(lblCena);
		
		txtCena = new JTextField();
		txtCena.setBounds(10, 250, 215, 20);
		contentPane.add(txtCena);
		txtCena.setColumns(10);
		
		JButton btnEdit = new JButton("Sacuvaj");
		btnEdit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				bookHelper book = new bookHelper(txtNaziv.getText().trim(), txtAutor.getText().trim(), txtGodIzdavanja.getText().trim(), txtZanr.getText().trim(), txtCena.getText().trim());
				int bookId = Integer.parseInt(id);
				dataHelper = new dbHelper();
				dataHelper.updateBook(book, bookId);
				JOptionPane.showMessageDialog(null, "Knjiga uspesno izmenjena!", "Info", JOptionPane.INFORMATION_MESSAGE);
				dispose();
			}
		});
		
		btnEdit.setBounds(10, 279, 89, 23);
		contentPane.add(btnEdit);
		
		JButton btnClose = new JButton("Izadji");
		btnClose.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				dispose();
			}
		});
		btnClose.setBounds(136, 279, 89, 23);
		contentPane.add(btnClose);
		
		bookId = id;
		popuniPolja();
		
	}
	
	public editFrame() {
		setTitle("Izmena knjige");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 255, 405);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		//dbHelper dataHelper = new dbHelper();
		
		JLabel lblNaziv = new JLabel("Naziv:");
		lblNaziv.setBounds(10, 64, 46, 14);
		contentPane.add(lblNaziv);
		
		txtNaziv = new JTextField();
		txtNaziv.setBounds(10, 81, 219, 20);
		contentPane.add(txtNaziv);
		txtNaziv.setColumns(10);
		
		JLabel lblAutor = new JLabel("Autor:");
		lblAutor.setBounds(10, 115, 46, 14);
		contentPane.add(lblAutor);
		
		txtAutor = new JTextField();
		txtAutor.setBounds(10, 135, 219, 20);
		contentPane.add(txtAutor);
		txtAutor.setColumns(10);
		
		JLabel lblGodIzdavanja = new JLabel("Godina Izdavanja:");
		lblGodIzdavanja.setBounds(10, 166, 110, 14);
		contentPane.add(lblGodIzdavanja);
		
		txtGodIzdavanja = new JTextField();
		txtGodIzdavanja.setBounds(10, 191, 219, 20);
		contentPane.add(txtGodIzdavanja);
		txtGodIzdavanja.setColumns(10);
		
		JLabel lblZanr = new JLabel("Zanr:");
		lblZanr.setBounds(10, 222, 46, 14);
		contentPane.add(lblZanr);
		
		txtZanr = new JTextField();
		txtZanr.setBounds(10, 247, 219, 20);
		contentPane.add(txtZanr);
		txtZanr.setColumns(10);
		
		JLabel lblCena = new JLabel("Cena");
		lblCena.setBounds(10, 278, 46, 14);
		contentPane.add(lblCena);
		
		txtCena = new JTextField();
		txtCena.setBounds(10, 303, 219, 20);
		contentPane.add(txtCena);
		txtCena.setColumns(10);
		
		JButton btnEdit = new JButton("Sacuvaj");
		btnEdit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
			}
		});
		
		btnEdit.setBounds(10, 332, 89, 23);
		contentPane.add(btnEdit);
		
		JButton btnClose = new JButton("Izadji");
		btnClose.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				dispose();
			}
		});
		btnClose.setBounds(136, 332, 89, 23);
		contentPane.add(btnClose);
		
		JLabel lblId = new JLabel("Id");
		lblId.setBounds(10, 11, 46, 14);
		contentPane.add(lblId);
		
		txtId = new JTextField();
		txtId.setBounds(10, 36, 219, 20);
		contentPane.add(txtId);
		txtId.setColumns(10);
		
		popuniPolja();
	}
	
	public void popuniPolja() {
		dftm = new DefaultTableModel();
		dbHelper dataHelper = new dbHelper();
		dftm = dataHelper.getEditBooks(bookId);
		
		for(int count = 0; count < dftm.getRowCount(); count++) {
			//txtId.setText(dftm.getValueAt(count, 0).toString());
			txtNaziv.setText(dftm.getValueAt(count, 1).toString());
			txtAutor.setText(dftm.getValueAt(count, 2).toString());
			txtGodIzdavanja.setText(dftm.getValueAt(count, 3).toString());
			txtZanr.setText(dftm.getValueAt(count, 4).toString());
			txtCena.setText(dftm.getValueAt(count, 5).toString());
		}
	}
}
