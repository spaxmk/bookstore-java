package bookstore;

import java.sql.*;

import javax.swing.table.DefaultTableModel;

public class dbHelper {
	
	private Connection connection;
	private Statement stmt;
	
	public dbHelper() {
		openConnection();
	}

	private void openConnection() {
		try {
			Class.forName("org.h2.Driver");
            connection = DriverManager.getConnection("jdbc:h2:~/test", "sa", "");
            stmt = connection.createStatement();
            String sql = "CREATE TABLE IF NOT EXISTS BOOKSTORE " 
            		+ "(id bigint auto_increment primary key, "
            		+ " naziv VARCHAR(255),"
                    + " autor VARCHAR(255), "
                    + " godIzdavanja VARCHAR(255), "
                    + " zanr VARCHAR(255),"
            		+ " cena VARCHAR(255) )";
            stmt.execute(sql);
            
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	
	public void closeConnection() {
        try {
        	stmt.close();
        	connection.close();
        } catch (Exception e) {
			e.printStackTrace();
        }
    }
	
	public void addNew(bookHelper book) {
		openConnection();
		try {
			String sql = "INSERT INTO BOOKSTORE (naziv, autor, godIzdavanja, zanr, cena) VALUES ( '" + book.getNaziv() + "', " + " '"+ book.getAutor() + "', "+ " '"+ book.getGodIzdavanja() +"', "+" '"+ book.getZanr()+"', '"+ book.getCena() +"')";
			stmt.execute(sql);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void delBook(int id) {
		try {
			String sql = "DELETE FROM BOOKSTORE WHERE id = " + id;
			stmt.execute(sql);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void updateBook(bookHelper book, int id) {
		try {
			String sql = "UPDATE BOOKSTORE SET naziv = '"+book.getNaziv()+"', autor = '"+book.getAutor()+"', godIzdavanja = '"+book.getGodIzdavanja()+"', zanr = '"+book.getZanr()+"', cena = '"+book.getCena()+"' "+"  WHERE id =  " + id ;
			stmt.execute(sql);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public DefaultTableModel getAllBooks() {
		String id = "", naziv = "", autor = "", godIzdavanja = "", zanr = "", cena = "";
		
		DefaultTableModel model;
		model = new DefaultTableModel();
		
		model.addColumn("ID");
		model.addColumn("Naziv");
		model.addColumn("Autor");
		model.addColumn("Godina izdavanja");
		model.addColumn("Zanr");
		model.addColumn("Cena");
		
		try {
			String sql = "SELECT * FROM BOOKSTORE";
			
			ResultSet rs = stmt.executeQuery(sql);
			while (rs.next()) {
				id = rs.getString("id");
				naziv = rs.getString("naziv");
				autor = rs.getString("autor");
				godIzdavanja = rs.getString("godIzdavanja");
				zanr = rs.getString("zanr");
				cena = rs.getString("cena");
				model.addRow(new Object[] {id, naziv, autor, godIzdavanja, zanr, cena});
			}
			return model;
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return model;
	}
	
	public DefaultTableModel getEditBooks(String bookId) {
		String id = "", naziv = "", autor = "", godIzdavanja = "", zanr = "", cena = "";
		

		DefaultTableModel tble;
		tble = new DefaultTableModel();
		
		tble.addColumn("ID");
		tble.addColumn("Naziv");
		tble.addColumn("Autor");
		tble.addColumn("Godina izdavanja");
		tble.addColumn("Zanr");
		tble.addColumn("Cena");
		
		try {
			String sql = "SELECT * FROM BOOKSTORE WHERE id = " + bookId;
			
			ResultSet rs = stmt.executeQuery(sql);
			while (rs.next()) {
				id = rs.getString("id");
				naziv = rs.getString("naziv");
				autor = rs.getString("autor");
				godIzdavanja = rs.getString("godIzdavanja");
				zanr = rs.getString("zanr");
				cena = rs.getString("cena");
				tble.addRow(new Object[] {id, naziv, autor, godIzdavanja, zanr, cena});
			}
			return tble;
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return tble;
		
	}
	
	public boolean provera(String keyword) {
        try {
            String sql = "SELECT * FROM BOOKSTORE WHERE naziv = '" + keyword + "'";
            ResultSet rs = stmt.executeQuery(sql);
            if (rs.next() == true) {
                return true;
            } else {
            	return false;
            }

            
        } catch (SQLException e) {
			e.printStackTrace();
        }
        return false;
    }
	
	
	
}
