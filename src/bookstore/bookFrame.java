package bookstore;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JButton;
import java.awt.Color;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.event.ActionEvent;
import javax.swing.JLabel;
import javax.swing.ImageIcon;
import java.awt.SystemColor;

@SuppressWarnings("serial")
public class bookFrame extends JFrame {

	private JPanel contentPane;
	private final dbHelper dbHelper;
	private JTable tblRes;
	
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					bookFrame frame = new bookFrame();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	public void ucitajPodatke() {    
		DefaultTableModel model;
		model = new DefaultTableModel(); 
		model = dbHelper.getAllBooks();
		tblRes.setModel(model);
	}
	
	/**
	 * Create the frame.
	 */
	public bookFrame() {
		setTitle("BookStore");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 643, 445);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		dbHelper = new dbHelper();
		
		addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent arg0) {
				dbHelper.closeConnection();
			}
		});
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(128, 11, 489, 384);
		contentPane.add(scrollPane);
		
		tblRes = new JTable();
		tblRes.setBackground(SystemColor.inactiveCaptionBorder);
		tblRes.setColumnSelectionAllowed(true);
		tblRes.setFillsViewportHeight(true);
		scrollPane.setViewportView(tblRes);
		
		JButton btnAddBook = new JButton("Dodaj");
		btnAddBook.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				addForm addFrm = new addForm();
				addFrm.setVisible(true);
				ucitajPodatke();
			}
		});
		btnAddBook.setBackground(Color.GREEN);
		btnAddBook.setBounds(10, 225, 108, 34);
		contentPane.add(btnAddBook);
		
		JButton btnEdit = new JButton("Izmeni");
		btnEdit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int row = tblRes.getSelectedRow();
				if(row == -1) {
					JOptionPane.showMessageDialog(null, "Niste izabrali knjigu koju zelite da izmenite!", "Info", JOptionPane.INFORMATION_MESSAGE);
				}
				else {
					String res = (String)tblRes.getValueAt(row, 0);
					editFrame edForm = new editFrame(res);
					edForm.setVisible(true);
					ucitajPodatke();
				}
				
			}
		});
		btnEdit.setBackground(Color.CYAN);
		btnEdit.setBounds(10, 270, 108, 34);
		contentPane.add(btnEdit);
		
		JButton btnDelete = new JButton("Obrisi");
		btnDelete.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int row = tblRes.getSelectedRow();
				if(row == -1) {
					JOptionPane.showMessageDialog(null, "Niste izabrali knjigu koju zelite da obrisete!", "Info", JOptionPane.INFORMATION_MESSAGE);
				}
				else {
					String bookId = (String)tblRes.getValueAt(row, 0);
					int id = Integer.parseInt(bookId);
					dbHelper.delBook(id);
					JOptionPane.showMessageDialog(null, "Knjiga je uspesno obrisana!", "Info", JOptionPane.INFORMATION_MESSAGE);
					ucitajPodatke();
				}
				
			}
		});
		btnDelete.setBackground(Color.RED);
		btnDelete.setBounds(10, 315, 108, 34);
		contentPane.add(btnDelete);
		
		JButton btnRefresh = new JButton("Osvezi");
		btnRefresh.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				ucitajPodatke();
			}
		});
		btnRefresh.setBounds(10, 361, 108, 34);
		contentPane.add(btnRefresh);
		
		JPanel panel = new JPanel();
		panel.setBounds(10, 11, 108, 105);
		contentPane.add(panel);
		
		JLabel lblNewLabel_1 = new JLabel("");
		lblNewLabel_1.setIcon(new ImageIcon(".\\Res\\image\\book.png"));
		panel.add(lblNewLabel_1);
		
		JLabel lblNewLabel = new JLabel("");
		lblNewLabel.setBounds(0, 0, 617, 406);
		contentPane.add(lblNewLabel);
		lblNewLabel.setIcon(new ImageIcon(".\\Res\\book.png"));
		
		ucitajPodatke();
	}
}
