package bookstore;

public class bookHelper {
	
	private String naziv;
	private String autor;
	private String godIzdavanja;
	private String zanr;
	private String cena;
	
	public bookHelper(String naziv, String autor, String godIzdavanja, String zanr, String cena) {
		this.naziv = naziv;
		this.autor = autor;
		this.godIzdavanja = godIzdavanja;
		this.zanr = zanr;
		this.cena = cena;
	}
	
	public String getNaziv() {
		return naziv;
	}
	
	public void setNaziv(String naziv) {
		this.naziv = naziv;
	}

	public String getAutor() {
		return autor;
	}

	public void setAutor(String autor) {
		this.autor = autor;
	}

	public String getGodIzdavanja() {
		return godIzdavanja;
	}

	public void setGodIzdavanja(String godIzdavanja) {
		this.godIzdavanja = godIzdavanja;
	}

	public String getZanr() {
		return zanr;
	}

	public void setZanr(String zanr) {
		this.zanr = zanr;
	}

	public String getCena() {
		return cena;
	}

	public void setCena(String cena) {
		this.cena = cena;
	}
	
}
