package bookstore;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import javax.swing.border.BevelBorder;
import java.awt.Toolkit;
import java.awt.SystemColor;
import javax.swing.UIManager;
import javax.swing.JTextField;
import java.awt.Font;
import java.awt.Image;

import javax.swing.JPasswordField;
import javax.swing.JButton;
import java.awt.Color;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.SwingConstants;
import java.awt.Component;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import javax.swing.ImageIcon;
import java.awt.Dimension;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class LogInFrame extends JFrame {

	
	private JPanel contentPane;
	private JTextField txtUsername;
	private JPasswordField txtPassword;
	/*
	private Image img_avatar = new ImageIcon(LogInFrame.class.getResource("E:\\Master\\I semestar\\Java\\Projekat\\Res\\avatarLogo.png")).getImage().getScaledInstance(90,90, Image.SCALE_DEFAULT);
	private Image img_user = new ImageIcon(LogInFrame.class.getResource("Res/usernameLogo.jpg")).getImage().getScaledInstance(90,90, Image.SCALE_SMOOTH);
	private Image img_password = new ImageIcon(LogInFrame.class.getResource("Res/passwordLogo.png")).getImage().getScaledInstance(90,90, Image.SCALE_SMOOTH);
	*/
	
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					LogInFrame frame = new LogInFrame();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public LogInFrame() {
		setUndecorated(true);
		setIconImage(Toolkit.getDefaultToolkit().getImage(".\\Res\\book.png"));
		
		setTitle("LogIn");
		setResizable(false);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 600, 400);
		contentPane = new JPanel();
		contentPane.setBackground(SystemColor.activeCaption);
		contentPane.setBorder(null);
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JPanel panel = new JPanel();
		panel.setBackground(UIManager.getColor("ScrollBar.background"));
		panel.setBounds(281, 0, 319, 400);
		contentPane.add(panel);
		panel.setLayout(null);
		
		JPanel panel_1 = new JPanel();
		panel_1.setBackground(UIManager.getColor("ScrollBar.thumb"));
		panel_1.setBounds(39, 172, 242, 49);
		panel.add(panel_1);
		panel_1.setLayout(null);
		
		txtUsername = new JTextField();
		txtUsername.addFocusListener(new FocusAdapter() {
			@Override
			public void focusGained(FocusEvent e) {
				if(txtUsername.getText().equals("Username:")) {
					txtUsername.setText(""); 
				}
				else {
					txtUsername.selectAll();
				}
			}
			@Override
			public void focusLost(FocusEvent e) {
				if(txtUsername.getText().equals("")){
					txtUsername.setText("Username:");
				}
			}
		});
		txtUsername.setBackground(UIManager.getColor("ScrollBar.thumb"));
		txtUsername.setBorder(null);
		txtUsername.setFont(new Font("Times New Roman", Font.BOLD, 14));
		txtUsername.setBounds(10, 11, 165, 27);
		panel_1.add(txtUsername);
		txtUsername.setColumns(10);
		
		JLabel lblUserIcon = new JLabel("");
		lblUserIcon.setHorizontalAlignment(SwingConstants.CENTER);
		lblUserIcon.setBounds(185, 0, 57, 49);
		ImageIcon imageIconUser = new ImageIcon(new ImageIcon(".\\Res\\usernameLogo.png").getImage().getScaledInstance(40, 40, Image.SCALE_DEFAULT));
		lblUserIcon.setIcon(imageIconUser);
		//lblUserIcon.setIcon(new ImageIcon(img_user));
		panel_1.add(lblUserIcon);
		
		JPanel panel_1_1 = new JPanel();
		panel_1_1.setBackground(UIManager.getColor("ScrollBar.thumb"));
		panel_1_1.setBounds(39, 232, 242, 49);
		panel.add(panel_1_1);
		panel_1_1.setLayout(null);
		
		// ●
		
		txtPassword = new JPasswordField();
		txtPassword.addFocusListener(new FocusAdapter() {
			@Override
			public void focusGained(FocusEvent e) {
				if(txtPassword.getText().equals("Password:")) {
					txtPassword.setEchoChar('●');
					txtPassword.setText("");
				}
				else 
					txtPassword.selectAll();
			}
			@Override
			public void focusLost(FocusEvent e) {
				if(txtPassword.getText().equals("")) {
					txtPassword.setText("Password:");
					txtPassword.setEchoChar((char)0);
				}
			}
		});
		txtPassword.setBackground(UIManager.getColor("ScrollBar.thumb"));
		txtPassword.setBorder(null);
		txtPassword.setFont(new Font("Times New Roman", Font.BOLD, 14));
		txtPassword.setBounds(10, 11, 166, 27);
		panel_1_1.add(txtPassword);
		
		JLabel lblPasswordIcon = new JLabel("");
		lblPasswordIcon.setHorizontalAlignment(SwingConstants.CENTER);
		lblPasswordIcon.setBounds(186, 0, 56, 49);
		ImageIcon imageIconPass = new ImageIcon(new ImageIcon(".\\Res\\passwordLogo.png").getImage().getScaledInstance(40, 40, Image.SCALE_DEFAULT));
		lblPasswordIcon.setIcon(imageIconPass);
		panel_1_1.add(lblPasswordIcon);
		
		JButton btnLogIn = new JButton("LOGIN");
		btnLogIn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(txtUsername.getText().trim().isEmpty() || txtPassword.getText().trim().isEmpty()) {
					JOptionPane.showMessageDialog(null, "Niste uneli sve neophodne podatke!", "Greška", JOptionPane.ERROR_MESSAGE);
				}
				else if (txtUsername.getText().equals("admin") && (txtPassword.getText().equals("admin")) ) {
					try {
						LogInFrame.this.dispose();
						bookFrame bookFrm = new bookFrame();
						bookFrm.setVisible(true);
					} catch (Exception e2) {
						e2.printStackTrace();
					}
				}
			}
		});
		btnLogIn.setForeground(new Color(255, 255, 255));
		btnLogIn.setFont(new Font("Times New Roman", Font.BOLD, 14));
		btnLogIn.setBackground(new Color(0, 128, 0));
		btnLogIn.setBounds(80, 303, 157, 32);
		panel.add(btnLogIn);
		
		JLabel lblNewLabel = new JLabel("X");
		lblNewLabel.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				if(JOptionPane.showConfirmDialog(null, "Zelite da napustite aplikaciju?", "Potvrda", JOptionPane.YES_NO_OPTION) == 0) {
					LogInFrame.this.dispose();
				}
			}
		});
		lblNewLabel.setAlignmentX(Component.CENTER_ALIGNMENT);
		lblNewLabel.setVerticalAlignment(SwingConstants.TOP);
		lblNewLabel.setFont(new Font("Trebuchet MS", Font.BOLD, 14));
		lblNewLabel.setBounds(305, 0, 14, 14);
		panel.add(lblNewLabel);
		
		JLabel lblAvatar = new JLabel("");
		lblAvatar.setHorizontalAlignment(SwingConstants.CENTER);
		lblAvatar.setMinimumSize(new Dimension(90, 90));
		lblAvatar.setMaximumSize(new Dimension(90, 90));
		lblAvatar.setSize(new Dimension(90, 90));
		//contentPane.add(lblAvatar);
		ImageIcon imageIcon = new ImageIcon(new ImageIcon(".\\Res\\avatarLogo.png").getImage().getScaledInstance(120, 120, Image.SCALE_DEFAULT));
		lblAvatar.setIcon(imageIcon);
		lblAvatar.setBounds(65, 34, 182, 139);
		panel.add(lblAvatar);
		
		JLabel lblBooksLogo = new JLabel("");
		lblBooksLogo.setBounds(10, 11, 261, 378);
		ImageIcon imageIconBooks = new ImageIcon(new ImageIcon(".\\Res\\pingBooks.png").getImage().getScaledInstance(250, 350, Image.SCALE_DEFAULT));
		lblBooksLogo.setIcon(imageIconBooks);
		contentPane.add(lblBooksLogo);
	}
}
