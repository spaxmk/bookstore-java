package bookstore;

import javax.swing.JPanel;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.EventQueue;
import java.awt.event.ActionEvent;

@SuppressWarnings("serial")
public class editForm extends JPanel {
	private JTextField txtNaziv;
	private JTextField txtAutor;
	private JTextField txtGodIzvrsavanja;
	private JTextField txtZanr;
	private JTextField txtCena;
	private dbHelper dataHelper;
	private DefaultTableModel dftm;
	
	public editForm() {
		setLayout(null);
		
		JLabel lblNaziv = new JLabel("Naziv:");
		lblNaziv.setBounds(10, 11, 46, 14);
		add(lblNaziv);
		
		txtNaziv = new JTextField();
		txtNaziv.setColumns(10);
		txtNaziv.setBounds(10, 28, 215, 20);
		add(txtNaziv);
		
		JLabel lblAutor = new JLabel("Autor:");
		lblAutor.setBounds(10, 62, 46, 14);
		add(lblAutor);
		
		txtAutor = new JTextField();
		txtAutor.setColumns(10);
		txtAutor.setBounds(10, 82, 215, 20);
		add(txtAutor);
		
		JLabel lblGodIzdavanja = new JLabel("Godina Izdavanja:");
		lblGodIzdavanja.setBounds(10, 113, 110, 14);
		add(lblGodIzdavanja);
		
		txtGodIzvrsavanja = new JTextField();
		txtGodIzvrsavanja.setColumns(10);
		txtGodIzvrsavanja.setBounds(10, 138, 212, 20);
		add(txtGodIzvrsavanja);
		
		JLabel lblZanr = new JLabel("Zanr:");
		lblZanr.setBounds(10, 169, 46, 14);
		add(lblZanr);
		
		txtZanr = new JTextField();
		txtZanr.setColumns(10);
		txtZanr.setBounds(10, 194, 212, 20);
		add(txtZanr);
		
		JLabel lblCena = new JLabel("Cena");
		lblCena.setBounds(10, 225, 46, 14);
		add(lblCena);
		
		txtCena = new JTextField();
		txtCena.setColumns(10);
		txtCena.setBounds(10, 250, 215, 20);
		add(txtCena);
		
		JButton btnEdit = new JButton("Sacuvaj");
		btnEdit.setBounds(10, 279, 89, 23);
		add(btnEdit);
		
		JButton btnClose = new JButton("Izadji");
		btnClose.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
			}
		});
		btnClose.setBounds(136, 279, 89, 23);
		add(btnClose);
	}

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					bookFrame frame = new bookFrame();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	
}
