## BookStore aplikacija radjena u svrhu polaganja ispita iz predmeta Java Programiranje.

## App Screenshot

![Login frame Screenshot](Res/scrh/login.png)
![Book frame Screenshot](Res/scrh/bookForm.png)
![Add book Screenshot](Res/scrh/Dodavanje-knjige.png)
![Error adding book Screenshot](Res/scrh/Neuspesno-dodavanje-knjige.png)
![Edit Screenshot](Res/scrh/Izmena.png)

### .jar aplikacije
Aplikacija se nalazi u Res/bookstore.jar
Za pokretanje aplikace neohodno je imati Java Runtime instaliranu na racunaru.

Username: admin
Password: admin

### Sadrzaj aplikacije
Aplikacija sadrzi tabelu sa informacijama vezanim za knjige
Mogucnost dodavanja, izmene i brisavanja knjiga.
Tabela se cuva u bazi podataka - H2 Database u sklopu .jar

## Aplikacija je u Beta verziji